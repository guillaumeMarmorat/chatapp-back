import express from 'express';
import http from 'http';
import io from 'socket.io';

const app = express();
const server = http.createServer(app);
const socketListener = io(server);

socketListener.on('connection', (socket) => {
    console.log('user connected');

    socket.on('message', (payload) => {
        let parsedPayload = JSON.parse(payload);
        if (parsedPayload._type === 1) {
            socketListener.emit('message', payload);
        }
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});


server.listen(3000, () => {
    console.log('Example app listening on port 3000!')
});

module.exports = app;
